#include "Windows.h"
#include "Helper_Types.h"
#include "Agent_Command.h"

// Always 1 less
#define DEFAULT_COMMANDS_COUNT 3

/*
// Global Command list
COMMAND CommandListHead[] = {
	{CommandListHead + 1, 0x0001, Terminate},
	{CommandListHead + 2, 0x0002, NULL},
	{CommandListHead + 3, 0x0003, NULL},
	{NULL, 0x0004, NULL}
};
*/
void RegisterCommand(USHORT code, WORK work)
{
	PCOMMAND command = GlobalCtx.CommandListHead + DEFAULT_COMMANDS_COUNT;
	
	while (command->next != NULL)
	{
		command = command->next;
	}

	command = &(command->next);

	command = (PCOMMAND)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(*command));
	command->code = code;
	command->work = work;
}

void UnregisterCommand(USHORT code)
{
	PCOMMAND command = GlobalCtx.CommandListHead + DEFAULT_COMMANDS_COUNT;
	PCOMMAND newNext;

	while (command->next != NULL)
	{
		if (command->next->code == code)
		{
			newNext = command->next->next;
			HeapFree(GetProcessHeap(), 0, (LPVOID)command->next);
			command->next = newNext;
			return;
		}
	}
}

DWORD Terminate(DWORD param)
{
	ExitProcess(0);

	return 0;
}