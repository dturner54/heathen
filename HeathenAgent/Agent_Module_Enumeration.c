#include "Windows.h"
#include "Agent_Module_Enumeration.h"

int System_Enumeration_Module()
{
	DWORD size, throwaway = 0;
	size = GetFileVersionInfoSizeA("kernel32.dll", &throwaway);
	LPVOID buffer = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, size);
	GetFileVersionInfoA("kernel32.dll", 0, size, buffer);
	LPVOID  lpBuffer = NULL;
	UINT puLen = 0;
	VerQueryValueA(buffer, "\\", &lpBuffer, &puLen);
	DWORD version = 0;
	version = ((VS_FIXEDFILEINFO *)lpBuffer)->dwProductVersionMS;
	WORD x = HIWORD(version);
	x = LOWORD(version);
	version = ((VS_FIXEDFILEINFO *)lpBuffer)->dwProductVersionLS;
	x = HIWORD(version);
	x = LOWORD(version);
	HeapFree(GetProcessHeap(), 0, buffer);

	return 0;
}