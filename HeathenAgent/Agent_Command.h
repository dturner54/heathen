#pragma once

typedef DWORD(*WORK)(PAGENTCTX param);

typedef struct _Command
{
	struct _Command *next;
	USHORT code;
	WORK work;

} COMMAND, *PCOMMAND;

//extern COMMAND CommandListHead[];

// Commmand Management Routines
void RegisterCommand(USHORT code, WORK work);
void UnregisterCommand(USHORT code);

// Command Work Routines

DWORD Terminate(DWORD param);

