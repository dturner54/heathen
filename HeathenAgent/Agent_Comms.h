#pragma once

typedef struct _HTTP_CTX
{
	HINTERNET hInternet;
	HINTERNET hOpen;
	DWORD timeout;
	LPCSTR url;
	PBYTE inputBuffer;
	PBYTE outputBuffer;
	DWORD iHeapSize;
	DWORD oHeapSize;

} HTTP_CTX, *PHTTP_CTX;

void HTTP_Init(PHTTP_CTX ctx, LPCSTR url);
//DWORD HTTP_Send();
DWORD HTTP_CheckIn(PHTTP_CTX ctx);
DWORD HTTP_ReadData(PHTTP_CTX ctx);
//DWORD HTTP_ConnectWithIp();
//DWORD HTTP_ConnectWithUrl();


