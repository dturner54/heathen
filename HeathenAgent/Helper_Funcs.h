#pragma once

void Helper_Zero_Memory(char *buff, SIZE_T size);
void Helper_Zero_Memory_Better(LPVOID buff, SIZE_T size);
void Helper_Copy_Memory(char *dst, char *src, SIZE_T size);