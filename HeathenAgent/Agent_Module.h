#pragma once

typedef struct _MODULE {

	struct _MODULE *next;
	SIZE_T size;
	BYTE modCode;
	DWORD (*code)(DWORD);

} MODULE, *PMODULE;

DWORD Agent_Install_Module(PBYTE modBuffer);
DWORD Agent_Uninstall_Module(BYTE modCode);
DWORD Agent_Execute_Module(BYTE modCode);
