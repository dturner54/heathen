#include "Windows.h"
#include "Agent_Encoding.h"

void Encode_Default(PBYTE buff, SIZE_T size)
{
	for (SIZE_T x = 1; x < size; x++)
	{
		buff[x] = _rotl8(buff[x], 1);
		buff[x] ^= buff[x - 1];
		buff[x] = _rotr8(buff[x], 3);
	}

	buff[0] ^= buff[size - 1];
}

void Decode_Default(PBYTE buff, SIZE_T size)
{
	buff[0] ^= buff[size - 1];

	for (SIZE_T x = size - 1; x > 0; x--)
	{
		buff[x] = _rotl8(buff[x], 3);
		buff[x] ^= buff[x - 1];
		buff[x] = _rotr8(buff[x], 1);
	}
}