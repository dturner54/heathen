#include "Windows.h"
#include "Helper_Types.h"
#include "Helper_Funcs.h"

void Helper_Zero_Memory(char *buff, SIZE_T size)
{
	for (SIZE_T x = 0; x < size; x++)
	{
		buff[x] = 0;
	}
}

void Helper_Copy_Memory(char *dst, char *src, SIZE_T size)
{
	for (SIZE_T x = 0; x < size; x++)
	{
		dst[x] = src[x];
	}
}

void Helper_Zero_Memory_Better(LPVOID buff, SIZE_T size)
{
	SIZE_T remainder = size % 4;
	SIZE_T moddedS = size / 4;
	SIZE_T x;

	if (remainder == 0)
	{
		for (x = 0; x < moddedS; x++)
		{
			*(((DWORD *)buff) + x) &= 0;
		}
	}

	else
	{
		for (x = 0; x < moddedS; x++)
		{
			*(((DWORD *)buff) + x) &= 0;
		}

		for (x = size - remainder; x < size; x++)
		{
			*(((CHAR *)buff) + x) &= 0;
		}
	}
}
