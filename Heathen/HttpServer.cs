﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Heathen
{
    public class HttpServer
    {
        MainModel mainModel;
        private System.Net.HttpListener listener;
        private System.Threading.Thread listenerThread;
        private System.Threading.ManualResetEvent stop;
        private bool isStarted;

        public HttpServer(MainModel main)
        {
            mainModel = main;
            listener = new System.Net.HttpListener();
            listenerThread = new System.Threading.Thread(HandleRequests);
            stop = new System.Threading.ManualResetEvent(false);
            isStarted = false;
        }

        public void Start()
        {
            listener.Prefixes.Add("http://+:80/");
            listener.Start();
            listenerThread.Start();
            isStarted = true;
        }

        public void Stop()
        {
            stop.Set();
            listenerThread.Join();
            listener.Stop();
        }

        public bool isRunning()
        {
            return isStarted;
        }

        private void HandleRequests()
        {
            while (listener.IsListening)
            {
                var result = listener.BeginGetContext(new AsyncCallback(ListenerCallback), listener);
                
                if (0 == System.Threading.WaitHandle.WaitAny(new System.Threading.WaitHandle[] { stop, result.AsyncWaitHandle }))
                    return;
            }
        }

        private void ProcessRequest(object stateinfo)
        {
            var context = stateinfo as System.Net.HttpListenerContext;
            var request = context.Request;

            mainModel.ServerLog = "[" + DateTime.UtcNow.ToString() + "] " + request.HttpMethod + request.Url.ToString();

            var response = context.Response;

            string responseString = "<HTML><BODY> Hello world!</BODY></HTML>";
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);

            response.ContentLength64 = buffer.Length;
            var output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);
            output.Close();

            /*
            switch (request.HttpMethod)
            {
                case "GET":
                    break;

                case "POST":
                    break;

                default:
                    break;      
            }
            */
        }

        private void ListenerCallback(IAsyncResult result)
        {
            var listener = result.AsyncState as System.Net.HttpListener;

            try
            {
                var context = listener.EndGetContext(result);
                System.Threading.ThreadPool.QueueUserWorkItem(ProcessRequest, context);
            }

            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.ToString(), "Exception", System.Windows.MessageBoxButton.OK);
            }
            
            
        }
    }
}
