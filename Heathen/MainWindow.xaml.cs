﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Heathen
{
    public partial class MainWindow : Window
    {
        // public static Dictionary<string, HttpServer> serverMap;
        private MainModel mainModel;
        private MainViewModel mainViewModel;

        public MainWindow()
        {
            mainModel = new MainModel();
            mainViewModel = new MainViewModel(mainModel);
            DataContext = mainViewModel;
            InitializeComponent();
        }

        private void ExitCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ExitCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        public static class CustomCommands
        {
            public static readonly RoutedUICommand Exit = new RoutedUICommand("Exit", "Exit", typeof(MainWindow));
        }
    }
}
