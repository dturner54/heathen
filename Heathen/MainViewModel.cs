﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Heathen
{
    public class MainViewModel : ObservableObject
    {
        private HttpServer server;
        private ICommand _startServerCommand;
        private ICommand _stopServerCommand;

        public MainViewModel(MainModel main)
        {
            Model = main;
            server = null;
        }

        public MainModel Model { get; set; }

        public ICommand StartServerCommand
        {
            get
            {
                if (_startServerCommand == null)
                {
                    _startServerCommand = new RelayCommand(param => StartServer(), param => server == null);
                }
                return _startServerCommand;
            }
        }

        public ICommand StopServerCommand
        {
            get
            {
                if (_stopServerCommand == null)
                {
                    _stopServerCommand = new RelayCommand(param => Stop(), param => server != null);
                }
                return _stopServerCommand;
            }
        }

        private void StartServer()
        {
            server = new HttpServer(Model);
            server.Start();
        }

        private void Stop()
        {
            server.Stop();
            server = null;
        }
    }
}
