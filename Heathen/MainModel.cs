﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heathen
{
    public class MainModel : ObservableObject
    {
        private string _serverLog;

        public string ServerLog
        {
            get { return _serverLog; }
            set
            {
                _serverLog += value + '\n';
                OnPropertyChanged("ServerLog");
            }
        }

    }
}
